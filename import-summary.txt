ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* .gitignore
* README.md
* TODO.md
* proguard.cfg
* studio/
* studio/.idea/
* studio/.idea/.name
* studio/.idea/compiler.xml
* studio/.idea/copyright/
* studio/.idea/copyright/profiles_settings.xml
* studio/.idea/encodings.xml
* studio/.idea/misc.xml
* studio/.idea/workspace.xml
* studio/app/
* studio/app/build.gradle
* studio/app/libs/
* studio/app/libs/jsoup-1.6.2.jar
* studio/app/lint.xml
* studio/app/src/
* studio/app/src/main/
* studio/app/src/main/AndroidManifest.xml
* studio/app/src/main/java/
* studio/app/src/main/java/com/
* studio/app/src/main/java/com/jlyr/
* studio/app/src/main/java/com/jlyr/JLyrMain.java
* studio/app/src/main/java/com/jlyr/JLyrSettings.java
* studio/app/src/main/java/com/jlyr/LyricBrowser.java
* studio/app/src/main/java/com/jlyr/LyricSearch.java
* studio/app/src/main/java/com/jlyr/LyricService.java
* studio/app/src/main/java/com/jlyr/LyricViewer.java
* studio/app/src/main/java/com/jlyr/preference/
* studio/app/src/main/java/com/jlyr/preference/JLyrSourceSelector.java
* studio/app/src/main/java/com/jlyr/preference/ProvidersPreference.java
* studio/app/src/main/java/com/jlyr/preference/ResetDisplayPreference.java
* studio/app/src/main/java/com/jlyr/preference/ResetStringPreference.java
* studio/app/src/main/java/com/jlyr/preference/TouchInterceptor.java
* studio/app/src/main/java/com/jlyr/providers/
* studio/app/src/main/java/com/jlyr/providers/AZLyricsProvider.java
* studio/app/src/main/java/com/jlyr/providers/ChartLyricsProvider.java
* studio/app/src/main/java/com/jlyr/providers/DarkLyricsProvider.java
* studio/app/src/main/java/com/jlyr/providers/DuckDuckGoProvider.java
* studio/app/src/main/java/com/jlyr/providers/DummyProvider.java
* studio/app/src/main/java/com/jlyr/providers/JamendoProvider.java
* studio/app/src/main/java/com/jlyr/providers/LyrDbProvider.java
* studio/app/src/main/java/com/jlyr/providers/LyricsProvider.java
* studio/app/src/main/java/com/jlyr/providers/MetroLyricsProvider.java
* studio/app/src/main/java/com/jlyr/providers/SongLyricsProvider.java
* studio/app/src/main/java/com/jlyr/receiver/
* studio/app/src/main/java/com/jlyr/receiver/AbstractPlayStatusReceiver.java
* studio/app/src/main/java/com/jlyr/receiver/AndroidMusicJRTStudioBuildReceiver.java
* studio/app/src/main/java/com/jlyr/receiver/AndroidMusicReceiver.java
* studio/app/src/main/java/com/jlyr/receiver/BuiltInMusicAppReceiver.java
* studio/app/src/main/java/com/jlyr/receiver/HeroMusicReceiver.java
* studio/app/src/main/java/com/jlyr/receiver/LastFmAPIReceiver.java
* studio/app/src/main/java/com/jlyr/receiver/LgOptimus4xReceiver.java
* studio/app/src/main/java/com/jlyr/receiver/MIUIMusicReceiver.java
* studio/app/src/main/java/com/jlyr/receiver/MyTouch4GMusicReceiver.java
* studio/app/src/main/java/com/jlyr/receiver/RdioMusicReceiver.java
* studio/app/src/main/java/com/jlyr/receiver/SEMCMusicReceiver.java
* studio/app/src/main/java/com/jlyr/receiver/SLSAPIReceiver.java
* studio/app/src/main/java/com/jlyr/receiver/SamsungMusicReceiver.java
* studio/app/src/main/java/com/jlyr/receiver/ScrobbleDroidMusicReceiver.java
* studio/app/src/main/java/com/jlyr/receiver/WinampMusicReceiver.java
* studio/app/src/main/java/com/jlyr/util/
* studio/app/src/main/java/com/jlyr/util/LyricReader.java
* studio/app/src/main/java/com/jlyr/util/Lyrics.java
* studio/app/src/main/java/com/jlyr/util/LyricsWebSearch.java
* studio/app/src/main/java/com/jlyr/util/NowPlaying.java
* studio/app/src/main/java/com/jlyr/util/ProvidersCollection.java
* studio/app/src/main/java/com/jlyr/util/Track.java
* studio/app/src/main/java/com/jlyr/util/TrackBrowser.java
* studio/app/src/main/java/cz/
* studio/app/src/main/java/cz/destil/
* studio/app/src/main/java/cz/destil/settleup/
* studio/app/src/main/java/cz/destil/settleup/gui/
* studio/app/src/main/java/cz/destil/settleup/gui/MultiSpinner.java
* studio/app/src/main/java/edu/
* studio/app/src/main/java/edu/gvsu/
* studio/app/src/main/java/edu/gvsu/masl/
* studio/app/src/main/java/edu/gvsu/masl/asynchttp/
* studio/app/src/main/java/edu/gvsu/masl/asynchttp/ConnectionManager.java
* studio/app/src/main/java/edu/gvsu/masl/asynchttp/HttpConnection.java
* studio/app/src/main/java/net/
* studio/app/src/main/java/net/margaritov/
* studio/app/src/main/java/net/margaritov/preference/
* studio/app/src/main/java/net/margaritov/preference/colorpicker/
* studio/app/src/main/java/net/margaritov/preference/colorpicker/AlphaPatternDrawable.java
* studio/app/src/main/java/net/margaritov/preference/colorpicker/ColorPickerDialog.java
* studio/app/src/main/java/net/margaritov/preference/colorpicker/ColorPickerPanelView.java
* studio/app/src/main/java/net/margaritov/preference/colorpicker/ColorPickerPreference.java
* studio/app/src/main/java/net/margaritov/preference/colorpicker/ColorPickerView.java
* studio/app/src/main/res/
* studio/app/src/main/res/drawable/
* studio/app/src/main/res/drawable/ic_launcher.png
* studio/app/src/main/res/layout-land/
* studio/app/src/main/res/layout-land/dialog_color_picker.xml
* studio/app/src/main/res/layout/
* studio/app/src/main/res/layout/about.xml
* studio/app/src/main/res/layout/dialog_color_picker.xml
* studio/app/src/main/res/layout/draggable_list_item.xml
* studio/app/src/main/res/layout/list_item.xml
* studio/app/src/main/res/layout/main.xml
* studio/app/src/main/res/layout/search.xml
* studio/app/src/main/res/layout/source_selector.xml
* studio/app/src/main/res/layout/viewer.xml
* studio/app/src/main/res/menu/
* studio/app/src/main/res/menu/browser.xml
* studio/app/src/main/res/menu/viewer.xml
* studio/app/src/main/res/values-fr/
* studio/app/src/main/res/values-fr/colorpicker_strings.xml
* studio/app/src/main/res/values-fr/notificationOptions.xml
* studio/app/src/main/res/values-fr/strings.xml
* studio/app/src/main/res/values/
* studio/app/src/main/res/values/colorpicker_integer.xml
* studio/app/src/main/res/values/colorpicker_strings.xml
* studio/app/src/main/res/values/dimens.xml
* studio/app/src/main/res/values/notificationOptions.xml
* studio/app/src/main/res/values/searchEngines.xml
* studio/app/src/main/res/values/strings.xml
* studio/app/src/main/res/xml/
* studio/app/src/main/res/xml/preferences.xml
* studio/build.gradle
* studio/gradle/
* studio/gradle/wrapper/
* studio/gradle/wrapper/gradle-wrapper.jar
* studio/gradle/wrapper/gradle-wrapper.properties
* studio/gradlew
* studio/gradlew.bat
* studio/local.properties
* studio/settings.gradle

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => app/src/main/AndroidManifest.xml
* libs/jsoup-1.6.2.jar => app/libs/jsoup-1.6.2.jar
* lint.xml => app/lint.xml
* res/ => app/src/main/res/
* src/ => app/src/main/java/

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
